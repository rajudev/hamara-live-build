`Config.d` directory consists of config files or parameters used for creating ISO.


The upstream documentation is at https://debian-live.alioth.debian.org/live-manual/stable/manual/html/live-manual.en.html

short description of some directory/files is given below :-

* **bootstrap**  -  set your distro name , (chroot,bootstrap,binary,security) mirror
* **common** - set your cache stages , system type (live/normal) , apt (ftp/http proxy), aptitude ,ISOhybrid , deboostrap options
* **chroot** - set keyring  file, enable/disable security/updates/backports , chroot filesystem
* **binary** - set boot/live-boot options , bootloader , debian-installer options(live,true,false,netboot etc) , debian-installer distribution,
        ISO metadata information etc.
* **build** - set image related information        
* **package-lists** - consists list of packages to install in mentioned stage  <file>.list.binary.chroot (installs packages in binary and chroot stage)
     or <file>.list.chroot (installs packages in chroot stage)
        live.list.chroot is default file which contains list of live boot packages.
        You can list packages in files with .list.chroot_live and .list.chroot_install suffixes inside the config/package-lists directory.
        If both a live and an install list exist, the packages in the .list.chroot_live list are removed with a hook after the installation (if the user uses the installer).
* **bootloaders** - consists different bootloaders directory (modify files in that bootloaders directory which you set in binary file)
* **archives** - area for  APT pinning (this will reuire if you want to install packages of different distribution). You can read more here https://debian-live.alioth.debian.org/live-manual/stable/manual/html/live-manual.en.html#apt-pinning
* **hooks** - allow commands to be performed in the chroot and binary stages of the build in order to customize the image.
* **includes** -  allow you to add or replace arbitrary files in your live system image. You can do this in two stages chroot and binary
  Use chroot.includes for modifying files in chroot stage and binary.includes for binary stage
* **preseed** - allows you to configure packages when they are installed by supplying answers to debconf questions.


# Steps for creating ISO

## First clone this repo

`git clone https://git.hamaralinux.org/hamara-developers/hamara-live-build.git`

## Change configuration according to your need before running `mkbuild`

* **`binary`** - set here boot paramerters , bootloader(grub/syslinux) , debian-installer mode and ISO metadata
* **`build`** - set your image/iso information
* **`bootstrap`** - set here your distribution name and mirror to fetch packages from for various stages.
* **`chroot`** - set your keyring package name (hamara-keyring) , linux kernel name and enable/disable updates,backport , security repo `Dont enable updates , backports as they are not available in namaste`
* **`bootloaders`** - edit cfg files according to your need in respective bootloader folder which was set in binary
* **`hooks`** - under live directory (our image type) write shell scripts for modifications to be reflected in final ISO . Extension .chroot indicates hooks to be run in chroot stage
* **`includes.<>`** - just put the files/folders in includes.'stage_name' you want to include in that stage
* **`includes.installer`** - consists preseed.cfg . Follow official guide https://www.debian.org/releases/stable/i386/apb.html
* **`package-lists`** - consists list of packages to install in mentioned stage  <file>.list.binary.chroot (installs packages in binary and chroot stage)
     or <file>.list.chroot (installs packages in chroot stage)
        live.list.chroot is default file which contains list of live boot packages.
        You can list packages in files with .list.chroot_live and .list.chroot_install suffixes inside the config/package-lists directory.
        If both a live and an install list exist, the packages in the .list.chroot_live list are removed with a hook after the installation (if the user uses the installer)


## Run script with sudo to build ISO

`# man ./HELP`

`# ./mkbuild iso ARCH MIRROR RELEASE VER`

* Valid options for ARCH :
`amd64` or `i386` or `all`

* Valid options for MIRROR :
`in.devel.hamaralinux.org` or `devel.hamaralinux.org`

* Valid options for RELEASE :
`hamara-sugam` or `hamara` 

* Valid options for VER :
`alpha` or `beta` or `final`

* Builds automagically using the live-build scripts which resides in `/usr/lib/live/build/`

Now , you will get output files in `output` directory.
