# Based on ubuntu-mate GRUB2 theme
#
# Copyright (C) 2017 I Sagar <sagar@hamaralinux.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# general settings
title-color: "#000"
title-font: "Terminus 14"
title-text: ""
desktop-image:"/boot/grub/live-theme/background.png"

# boot menu
+ boot_menu {
    left = 25%
    width = 50%
    top = 20%
    height = 40%
    item_font = "Terminus 18"
    item_color = #000"
    selected_item_font = "Terminus 18"
    selected_item_color = "#fff"
    icon_width = 14
    icon_height = 14
    item_height = 22
    item_padding = 40
    item_icon_space = 0
    item_spacing = 0
    scrollbar = true
    scrollbar_width = 20
    scrollbar_thumb = "slider_*.png"
    menu_pixmap_style = "menu_frame_*.png"
    selected_item_pixmap_style = "highlight_*.png"
}

# progress bar
+ progress_bar {
    id = "__timeout__"
    left = 25%
    width = 50%
    top = 80%
    height = 20
    show_text = true
    font = "Terminus 18"
    text_color = "#fff"
    text = "@TIMEOUT_NOTIFICATION_MIDDLE@"
    bar_style = "progress_frame_*.png"
    highlight_style = "progress_highlight_*.png"
}

# help bar at the bottom
+ hbox {
    top = 100%-35
    left =40%
    width = 100%
    height = 20
    font = "Terminus 18" 
    + label { text = "enter" color = "#f79421" }
    + label { text = ":boot    " color = "#000" }
    + label { text = "e" color = "#f79421" }
    + label { text = ":edit    "  color = "#000" }
    + label { text = "c" color = "#f79421" }
    + label { text = ":command-line" color = "#000" }
}
